package utils;

import models.Path;
import models.Point;

import java.util.*;

public class utils {
    private static Random generator;

    private static int generateCoordinate(){
        generator = new Random();
        return generator.nextInt(11)+1;
    }

    private static Float length(Point pointA, Point pointB){

        int dX = pointB.getCoordinateX() - pointA.getCoordinateX();
        int dY = pointB.getCoordinateY() - pointA.getCoordinateY();

        return (float)Math.sqrt(dX * dX + dY * dY);
    }

    private static List<Path> getPointAllPaths(Point point){
        List<Path> pointPaths = new ArrayList<Path>();

        for(Point iter_point: point.getConnectionToPoints()){
            Path somePath = new Path();
            somePath.addPoint(point);
            if(!somePath.getPath().contains(iter_point)){

                if(somePath.getLastPoint().getConnectionToPoints().contains(iter_point)) {
                    somePath.addPoint(iter_point);
                }
            }
            for(Point iter_inner_point: iter_point.getConnectionToPoints()){
                if(!somePath.getPath().contains(iter_inner_point)){
                    if(somePath.getLastPoint().getConnectionToPoints().contains(iter_inner_point)) {
                        somePath.addPoint(iter_inner_point);
                    }
                }
                for(Point iter_inner_2_point: iter_point.getConnectionToPoints()){
                    if(!somePath.getPath().contains(iter_inner_2_point)){
                        if(somePath.getLastPoint().getConnectionToPoints().contains(iter_inner_2_point)) {
                            somePath.addPoint(iter_inner_2_point);
                        }
                    }

                    for(Point iter_inner_3_point: iter_point.getConnectionToPoints()){
                        if(!somePath.getPath().contains(iter_inner_3_point)){
                            if(somePath.getLastPoint().getConnectionToPoints().contains(iter_inner_3_point)) {
                                somePath.addPoint(iter_inner_3_point);
                            }
                        }
                        for(Point iter_inner_4_point: iter_point.getConnectionToPoints()){
                            if(!somePath.getPath().contains(iter_inner_4_point)){
                                if(somePath.getLastPoint().getConnectionToPoints().contains(iter_inner_4_point)) {
                                    somePath.addPoint(iter_inner_4_point);
                                }
                            }
                            for(Point iter_inner_5_point: iter_point.getConnectionToPoints()){
                                if(!somePath.getPath().contains(iter_inner_5_point)){
                                    if(somePath.getLastPoint().getConnectionToPoints().contains(iter_inner_5_point)) {
                                        somePath.addPoint(iter_inner_5_point);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Point prev_point = new Point();
            for(Point len_point : somePath.getPath()){
                somePath.setLength(somePath.getLength() + length(prev_point, len_point));
                prev_point = len_point;
            }

            pointPaths.add(somePath);
        }

        return pointPaths;
    }

    private static Path getBestPointPath(Point point){
        List<Path> pointPaths = getPointAllPaths(point);
        System.out.println("How many paths found for point " + point.getName() + ": " + pointPaths.size());
        Path bestPointPath = new Path();

        for(Path path : pointPaths){
            if(path.getPathPoints() > bestPointPath.getPathPoints()){
                bestPointPath = path;
            }
            if(path.getPathPoints() == bestPointPath.getPathPoints() && path.getLength() < bestPointPath.getLength()){
                bestPointPath = path;
            }
        }

        return bestPointPath;
    }

    public static List<Point> createPoints(int howManyPoints, int maxEdgeValue) {
        if(howManyPoints < maxEdgeValue){
            maxEdgeValue = howManyPoints - 1;
        }
        List<Point> newPoints = new ArrayList<Point>();

        for(int i = 0; i < howManyPoints; i++){
            Point temp_point = new Point("Point " + i, generateCoordinate(), generateCoordinate());

            if(!newPoints.contains(temp_point)){
                newPoints.add(temp_point);
            }
        }

        for(int k = 0; k < newPoints.size(); k++){
            int edgesNumber = generator.nextInt(maxEdgeValue) + 1;
            List<Point> tempPointConnections = new ArrayList<Point>();
            List<Integer> indexes = new ArrayList<Integer>();
            for(int e = 0; e < edgesNumber; e++) {
                int connIndex = generator.nextInt(newPoints.size());
                if(!indexes.contains(connIndex) && connIndex != k){
                    tempPointConnections.add(newPoints.get(connIndex));
                    indexes.add(connIndex);
                }
            }
            Point point = newPoints.get(k);
            point.setConnectionToPoints(tempPointConnections);

            System.out.println("Point: " + point.fullToString());
        }
        return newPoints;
    }

    public static void countPath(Set<Point> pointCollection){
        List<Path> paths = new ArrayList<Path>();

        Path bestPath = new Path();

        for(Point point : pointCollection){
            paths.add(getBestPointPath(point));
        }

        System.out.println("Returned paths: " + paths.size());

        for(Path path : paths){
            if(path.getPathPoints() > bestPath.getPathPoints()){
                bestPath = path;
            }
            if(path.getPathPoints() == bestPath.getPathPoints() && path.getLength() < bestPath.getLength()){
                bestPath = path;
            }
        }

        System.out.println("Najlepsza znaleziona ścieżka: " + bestPath.fullToString());
    }
}
