import models.Point;

import java.util.*;

import static utils.utils.countPath;
import static utils.utils.createPoints;

public class Main {

    public static void main(String[] args) {
        System.out.println("********************************");
        System.out.println("*========== Początek ==========*");
        System.out.println("********************************");

        int howManyPoints = 6;
        int howManyEdges = 4;

        Set<Point> points = new HashSet<Point>(createPoints(howManyPoints, howManyEdges));

        countPath(points);
        System.out.println();
        System.out.println();

        System.out.println("********************************");
        System.out.println("*=========== Koniec ===========*");
        System.out.println("********************************");
    }
}
