package models;

import java.util.ArrayList;
import java.util.List;

public class Point {

    private String name;
    private int coordinateX;
    private int coordinateY;
    private List<Point> connectionToPoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public List<Point> getConnectionToPoints() {
        return connectionToPoints;
    }

    public void setConnectionToPoints(List<Point> connectionToPoints) {
        this.connectionToPoints = connectionToPoints;
    }

    public Point(){}

    public Point(String name, int coordinateX, int coordinateY) {
        this.name = name;
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public Point(String n, int x, int y, List<Point> p){
        this.name = n;
        this.coordinateX = x;
        this.coordinateY = y;
        this.connectionToPoints = p;
    }

    @Override
    public String toString() {
        return "Point{" +
                "name='" + name + '\'' +
                ", coordinateX=" + coordinateX +
                ", coordinateY=" + coordinateY +
                '}';
    }

    public String fullToString() {
        return "Point{" +
                "name='" + name + '\'' +
                ", coordinateX=" + coordinateX +
                ", coordinateY=" + coordinateY +
                ", connections=" + connectionToPoints +
                '}';
    }
}
