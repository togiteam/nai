package models;

import java.util.ArrayList;
import java.util.List;


public class Path {
    private float length;
    private List<Point> path;

    public Path() {
        this.length = 0;
        this.path = new ArrayList<Point>();
    }

    public Path(int length, List<Point> path) {
        this.length = length;
        this.path = path;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public List<Point> getPath() {
        return path;
    }

    public void setPath(List<Point> path) {
        this.path = path;
    }

    public int getPathPoints(){
        return this.path.size();
    }

    public void addPoint(Point point){
        this.path.add(point);
    }

    public Point getLastPoint(){
        int pathPointCount = this.path.size();

        return this.path.get(pathPointCount - 1);
    }

    @Override
    public String toString() {
        return "Path{" +
                " length=" + length +
                " }";
    }

    public String fullToString(){
        return "Path{" +
                " length=" + length +
                ", how many points will be visited: " + path.size() +
                ", path=" + path +
                " }";
    }
}
